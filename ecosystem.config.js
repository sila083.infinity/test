module.exports = {
         apps: [
           {
             name: "test",
             script: "serve",
             env: {
               PM2_SERVE_PATH: "./public",
               PM2_SERVE_PORT: 8002,
               PM2_SERVE_SPA: "true",
               PM2_SERVE_HOMEPAGE: "/index.html",
             },
             exec_mode: "cluster",
           },
         ],
       };
       